# RPS project website
The RPS project website is built using VitePress and runs in a docker container.

The documentation is not stored inside this repository, it will include the documentation from the RPS Portal repository.

## Getting started with local development

For local development it is enough to use the `/src` folder and work within it ignoring all the rest. The remote deployment will happen automatically (see related section below).

### Use docker-compose (recommended)
To get a development server running in a docker container, run the following commands:
```bash
docker-compose --profile dev up
```


Go to http://localhost:5173 to preview the site.

The documentation for the subprojects is mounted from the project repos one level above the rps-website folder, e.g. ../rps-portal/docs will be mounted to /src/docs/portal by docker-compose. Edit files there.

### Run without docker
To get a development server running, run the following commands:
```bash
yarn install
yarn dev
```

Go to http://localhost:5173 to preview the site.

## Previewing the production build

To preview the production build in a docker container, run the following commands:
```bash
docker-compose --profile prod up
```

Go to http://localhost:5174 to view the build of the site.

## Deployment

Once the local development is ready, the version in the main branch will get build by Gitlab to an Docker Image. This image will be deployed automatically to the server and the website is then available at https://research-project-suite.org

So you only need to push your changes to the main branch and the rest will happen automatically!

To redeploy the website with traefik and the whole setup, run the following commands:
```bash
ansible-playbook -i inventory/ site.yaml
```
