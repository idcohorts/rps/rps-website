FROM node:18
WORKDIR /srv/rps-website
COPY package.json yarn.lock ./
RUN yarn install
COPY . .

RUN git clone -b main https://gitlab.com/idcohorts/rps/rps-portal.git /srv/rps-portal
RUN cp -a /srv/rps-portal/docs/. /srv/rps-website/src/docs/portal/

RUN yarn build

FROM nginx:1
COPY --from=0 /srv/rps-website/.vitepress/dist/ /usr/share/nginx/html
