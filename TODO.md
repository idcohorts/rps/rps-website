# ToDo RPS Website

Mark with ✅ if something is ready

- [ ] Sponsor Logos on the home page (BMBF, NUM, IDcohorts)
- [ ] Nice home page
- [ ] Feature texts
    - [ ] Open Source Colab Tools bundled to a single platform for easy use in research projects
    - [ ] Unified navigation and search accross all tools
    - [ ] Single sign-on: One login for all services
    - [ ] Unified deployment: One command deployment of all services
        - "You don't need to write a bunch of docker-compose files, we did that for you"
- [ ] Nice contributors page
    - [ ] Sponsor section
    - [ ] Developer section
    - [ ] Upstream software projects
- [ ] Nice about page
- [ ] Slideshow with reveal.js on /slides
- [ ] Two documentation sections for users and developes
- [ ] Blue theming
- [ ] RPS header integration
- [ ] Blog section
- [ ] 3 newest blog posts on home page
- [ ] Edit link on every page to edit in gitlab


# Content-Related ToDo List:

Mark with ✅ if something is ready

-----------------------------
## Tasks:
- [x] Prepare the website scheme: where and how is styling defined, where are static pages stored, the content (such as pictures)
- [x] Fill the skeleton with dummy pages
- [ ] Fill with content
- [ ] Replace digital placeholders

----------------
## Content:

### Header:
- [x] Logo
- [x] Navigation

### Footer:
- [x] "Impressum"
- [x] Our Team (link to the Contributors page)
- [ ] feedback: social media customizable units, Email unit with customizable text (default: feedback), "Found a Bug?"

### Body:

#### Main page 
- [x] Title and a moto
- [ ] Eye-catcher: a picture or a graph with schematic representation of the ideology of the project
- [x] Buttons: Download (with the current version tag), Quick guide, Live examples
- [x] 3 Features: short tags with some description on why RPS is a must-have tool, maybe with icons
- [x] Main text with the general description (best with iconic sketches):


#### Contributors page
- [x] a list of contributors, maybe a group photo with as many of them as we meet at once

#### Download page
- [x] a table with the current version on top, a few latest below (placeholder for now): link to git repo
- [ ] a clap button with the release changelog for each version
- [ ] quick instruction on how to use the git repo (yes, many developers still don't know git and are afraid of it)
- [ ] counter of downloads and forks (if applicable)

####  Found a Bug?
- [x] a short message like: "As in any big project, we might expect some bugs to be in the RPS, too. We are happy to hear them reported by you, so don't hesitate to support us with your sharp eye!"
- [ ] a list of known bugs to be fixed soon. The security vulnerabilities are NOT listed here
- [ ] a from to fill and send an Email.

#### Manuals/users
- [x] a message that this page is not only for a user, but also for a developer to get a quick and good feel about the possible user experience
- [x] quick overview of the services
- [ ] a simple graph with the overview of the services

#### Manuals/admins
- [ ] how to set up the system, what commands can improve the default configs, how to manage users
- [ ] a simple graph of the services network

#### Manuals/developers
- [ ] how to customize the services deeper, how to add services, how is reverse proxy configured
- [ ] a deep graph of the services network