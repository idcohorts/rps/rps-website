---
title: Feedback
---

<div class="contact-form">
  <form>
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" required>
    <label for="email">Email:</label>
    <input type="email" id="email" name="email" required>
    <label for="message">Message:</label>
    <textarea id="message" name="message" required></textarea>
    <button type="submit" onclick="submitForm()">Send</button>
  </form>
</div>

<script>
function submitForm() {
  const name = document.getElementById('name').value;
  const email = document.getElementById('email').value;
  const message = document.getElementById('message').value;

  const mailtoLink = `mailto:christian.akele@uk-koeln.de?subject=Feedback&body=Name:%20${name}%0DEmail:%20${email}%0DMessage:%20${message}`;

  window.open(mailtoLink);
}
</script>