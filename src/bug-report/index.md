---
layout: docs
title: "Report Bug"
---

# Found a bug?

If you found something that doesn't seem quite right - let us know!

  <h2>Contact Form</h2>
    <form action="https://getform.io/f/531e5b72-d783-4629-9ea9-70842e55df02" method="POST">
    <h3>Name</h3>
    <input type="text" name="name">
    <h3>Email</h3>
    <input type="email" name="email">
    <h3>Message</h3>
    <input type="text" name="message">
    <!-- add hidden Honeypot input to prevent spams -->
    <input type="hidden" name="_gotcha" style="display:none !important">
    <br><button type="submit">Send</button>
</form>

<style scoped>
  input[type="text"], input[type="email"] {
    border: 1px solid #ccc;
    font-size: 1rem;
    padding: 6px 10px;
    border-radius: 4px;
  }
  button[type="submit"] { 
    width: 215px;
    margin: 1em auto;
    background-color: rgb(67 56 202);
    color: white;
    font-size: 0.8rem;
    border: none;
    border-radius: 4px;
    padding: 8px 12px;
    font-weight: 500;
  }
</style>