---
layout: docs
title: "Impressum"
---
Prof. Dr. med. Jörg-Janne Vehreschild

Weißhausstr. 24<br>
50939 Köln<br>
GERMANY

Contact: 
- Telefon: +49 221 47888794
- Telefax: +49 221 4783611
- E‑Mail: support@idcohorts.net
