---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
title: RPS Home

hero:
  name: Research Project Suite
  # text: "Project website of the Research Project Suite"
  tagline: One-click-deployable and complete!
  # image:
  #   src: /rps_schema.png
  #   alt: RPS
  actions:
    # - theme: alt
    #   text: Quick guide
    #   details: Get started with our handy guides and examples
    #   link: /404/
    - theme: brand
      text: Documentation
      details: Inspect the inner kitchen fundamentally
      link: /docs/portal/
    - theme: alt
      text: Source code
      details: Get the latest version
      link: /download/
    - theme: alt
      text: Poster (German PDF)
      details: get an overview of the project
      link: /rps-napkonvention-poster-2023.pdf

features:
  - title: Open-Source and free
    details: RPS is non-commercial and aims to be as clear to use as possible. All components are open-source as well as the RPS code itself
  - title: Scalable
    details: RPS is designed to fulfil the requirements of a big network while at the same keeping itself comfortable to be used internally in a small research group
  - title: Flexible
    details: The components of the Suite are clearly separated from each other making it easy to manage each one without risking to break the others
---

<div class="spacer"></div>

<div id="horizontal_block" style="height:100%; width:100%; overflow: hidden;">
  <div id="left_schema" style="float: left; width:25%;">
    <img src="/rps_logo.svg" align="middle">
  </div>
  <div id="right_content" style="float: left; width:60%;">
    <div class="content-wrap" id="about">
      <p>The Research Project Suite (RPS), as comes from its name, is a collection of tools for managing research projects. The goal is to provide a collection of practical tools to give a level-up for your research environment. Although our solution is highly customisable, it is already pre-configured by default and all you need to start using it is to deploy it with a few simple steps.</p>
      <h1>The RPS is a work in progress continuously improving and providing a better and bigger stack of features and applications. The current version combines the following services:</h1>
      <ul>
        <li>- <b>Keycloak</b>        to provide a centralized authentication</li>
        <li>- <b>Nextcloud</b>       to store and share your data</li>
        <li>- <b>Discourse</b>       to have structured discussions</li>
        <li>- <b>Matrix/Element</b>      to communicate with the suite members</li>
        <li>- <b>Bookstack (Wiki)</b>  to organize knowledge</li>
        <li>- <b>RPS Groups Interface</b> to manage group memberships</li>
        <li>- <b>RPS Admin Tools</b> to manage users and permissions</li>
        <li>- <b>RPS Header</b> to provide a unified navigation across services</li>
      </ul>
    </div> 
  </div>
  <img src="/diagram.svg" style="padding: 100px;">
</div>
