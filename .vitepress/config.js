import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
    appearance: false, // if false, only light theme is allowed: https://vitepress.dev/reference/site-config#appearance
    srcDir: './src',
    head: [
        ['script', {src: 'https://code.jquery.com/jquery-3.4.1.min.js'}]
    ],
    title: 'Research Project Suite',
    ignoreDeadLinks: true,
    // description: 'Project website of the Research Project Suite',
    themeConfig: {
        logo: '/rps_logo.svg',
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            { text: 'Home',          link: '/' },
            { text: 'About RPS',     link: '/#about' },
            { text: 'Download',      link: '/download/' },
            { text: 'Documentation', link: '/docs/portal/' },
        ],

        sidebar: [
            {
                text: 'Menu',
                items: [
                    { text: 'Documentation', link: '/docs/portal/' },
                    { text: 'Download',      link: '/download/' },
                    { text: 'Found a bug?',  link: '/bug-report' },
                ]
            }
        ],

        footer: {
          message: '<a href="/team/">Our Team</a>    |    <a href="/impressum/">Impressum</a> |    <a href="/bug-report/">Found a bug?</a>',
          copyright: 'Copyright © 2022-present by <a href="https://idcohorts.net/">IDCohorts</a>',
        },
        // socialLinks: [
        //    { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
        // ],

    },
    // srcExclude: [
    //    './README.md',
    //    './TODO.md',
    // ],
    cleanUrls: true,
})
